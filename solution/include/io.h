#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include <stdbool.h>
#include <stdio.h>

bool open_file (const char* path, FILE** file, const char* mode);

bool close_file (FILE* file);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
