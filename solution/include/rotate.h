#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
