#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>

#include "pixel.h"

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image image_init (const uint32_t width, const uint32_t height);

void image_free (struct image* image);

bool set_pixel (struct image* image, const struct pixel pixel, const uint32_t x, const uint32_t y);

struct pixel get_pixel (const struct image* image, const uint32_t x, const uint32_t y);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
