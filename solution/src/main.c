#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "io.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Программа должна запускаться в формате './image-transformer input_file output_file'\n");
        return 1;
    }
    FILE* in = NULL;
    FILE* out = NULL;
    struct image image = {0};
    if (open_file(argv[1], &in, "rb")) {
        if (from_bmp(in, &image) == READ_OK) {
            struct image new_image = rotate(image);
            if (open_file(argv[2], &out, "wb")) {
                if (to_bmp(out, &new_image) == WRITE_OK) {
                    image_free(&image);
                    image_free(&new_image);
                    return 0;
                } else {
                    fprintf(stderr, "Не удалось записать выходной файл");
                }
            } else {
                fprintf(stderr, "Не удалось открыть выходной файл");
            }
            image_free(&image);
            image_free(&new_image);
        } else {
            fprintf(stderr, "Не удалось считать структуру bmp");
        }
    } else {
        fprintf(stderr, "Не удалось открыть входной файл");
    }
    close_file(in);
    close_file(out);
    return 1;
}
