#include "rotate.h"

#include "image.h"
#include "pixel.h"

struct image rotate( struct image const source ) {
    struct image new_img = image_init(source.height, source.width);
    for (int32_t i = 0; i < source.height; i++) {
        for (int j = 0; j < source.width; j++) {
            struct pixel pixel = get_pixel (&source, j, source.height - i - 1);
            set_pixel (&new_img, pixel, i, j);
        }
    }
    return new_img;
}
