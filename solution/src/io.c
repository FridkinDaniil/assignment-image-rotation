#include "io.h"

#include <stdio.h>

bool open_file (const char* path, FILE** file, const char* mode){
    if (path != NULL) {
        *file = fopen(path, mode);
        if (*file != NULL) {
            return true;
        }
    }
    return false;
}

bool close_file (FILE* file){
    if (file != NULL) {
        if (fclose(file) == 0) {
            return true;
        }
    }
    return false;
}
