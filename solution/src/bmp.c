#include "bmp.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

#define BFTYPE 0x4D42
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24


struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool validate_signature(const struct bmp_header header) {
    return header.bfType == BFTYPE;
}

static void make_padding ( FILE* in, struct image* img) {
    if ((img->width * 3) % 4 != 0) {
                fseek(in, 4 - ((img->width * 3) % 4), SEEK_CUR);
            }
}

static bool make_image ( FILE* in, const struct bmp_header header, struct image* img) {
    *img = image_init(header.biWidth, header.biHeight);
    fseek(in, header.bOffBits, SEEK_SET);
    for (int32_t i = 0; i < img->height; i++) {
        for (int32_t j = 0; j < img->width; j++) {
            struct pixel pixel = {0};
            if (fread(&pixel, sizeof (struct pixel), 1, in) == 0) {
                image_free(img);
                return false;
            }
            set_pixel (img, pixel, j, i);
        }
        make_padding(in, img);
    }
    return true;
}

static uint32_t calculate_bfileSize (const struct image* img) {
    return B_OFF_BITS + img->width * img->height * sizeof(struct pixel)
                       + img->height * ((img->width * 3) % 4 != 0 ? 4 - ((img->width * 3) % 4) : 0);
}

static struct bmp_header make_header(const struct image* img) {
    struct bmp_header header = {0};
    header.bfType = BFTYPE;
    header.bfileSize = calculate_bfileSize(img);
    header.bOffBits = B_OFF_BITS;
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biSizeImage = header.bfileSize - B_OFF_BITS;
    return header;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (in != NULL) {
        struct bmp_header bmp_header = {0};
        if (fread(&bmp_header, sizeof (struct bmp_header), 1, in) != 0) {
            if (validate_signature(bmp_header)) {
                if (make_image(in, bmp_header, img)) {
                    return READ_OK;
                }
            } else {
                return READ_INVALID_SIGNATURE;
            }
        } else {
            return READ_INVALID_HEADER;
        }
    }
    return READ_INVALID_BITS;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header bmp_header = make_header(img);
    if (fwrite(&bmp_header, sizeof (struct bmp_header), 1, out) != 0) {
        for (int32_t i = 0; i < img->height; i++){
            for (int32_t j = 0; j < img->width; j++) {
                struct pixel pixel = get_pixel (img, j, i);
                if (fwrite(&pixel, sizeof (struct pixel), 1, out) == 0) {
                    return WRITE_ERROR;
                }
            }
            int8_t zero = 0;
            if ((img->width * 3) % 4 != 0) {
                for (size_t k = 0; k < 4 - ((img->width * 3) % 4); k++){
                    if(fwrite(&zero, sizeof (int8_t), 1, out) == 0) {
                        return WRITE_ERROR;
                    }
                }
            }
        }
    } else {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
