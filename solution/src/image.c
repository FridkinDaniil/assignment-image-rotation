#include "image.h"

#include <inttypes.h>
#include <malloc.h>

#include "pixel.h"

struct image image_init (const uint32_t width, const uint32_t height) {
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc (sizeof (struct pixel) * width * height);
    return image;
}

void image_free (struct image* image) {
    free(image->data);
}

bool set_pixel (struct image* image, const struct pixel pixel, const uint32_t x, const uint32_t y) {
    if (x < image->width && y < image->height) {
        *(image->data + y * image->width + x) = pixel;
        return true;
    }
    return false;
}

struct pixel get_pixel (const struct image* image, const uint32_t x, const uint32_t y) {
    return *(image->data + y * image->width + x);
}
